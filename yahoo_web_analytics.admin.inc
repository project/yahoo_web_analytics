<?php

/**
 * @file
 * Include file for Yahoo! Web Analytics module.
 */


/**
 * Admin settings form.
 */
function admin_settings_form() {

  $form['general_tracking_settings'] = array(
    '#title' => t('General Tracking Settings'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $form['general_tracking_settings']['yahoo_web_analytics_tracking_id'] = array(
    '#title' => t('Yahoo! Web Analytics Tracking ID'),
    '#type' => 'textfield',
    '#description' => t('The Yahoo! Web Analytics tracking ID number provided in the installation tracking code.  View the tracking code by clicking <strong>Installation</strong> within the Yahoo! Web Analytics user interface and then selecting the type of tracking code.  The ID will be present within the JavaScript: YWA.getTracker("..."). You can obtain an account from the !yahoo_link website.', array('!yahoo_link' => l(t('Yahoo! Web Analytics'), 'http://web.analytics.yahoo.com/', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => variable_get('yahoo_web_analytics_tracking_id', ''),
    '#required' => TRUE,
    '#size' => 20,
    '#maxlength' => 20,
  );

  // Custom fields (Session) 01-06.
  $form['custom_session_field_fieldset'] = array(
    '#title' => t('Custom Fields (Session)'),
    '#type' => 'fieldset',
    '#description' => t('The fields below allow specific session variable values to be tracked throughout the site.<p><strong>Example:</strong<br />If the number of comments per page was to be tracked, the session variable name (e.g. comment_comments_per_page) would be placed into a field below, then properly configured within the Yahoo! Web Analytics user interface.  The session variable\'s value would then be placed into the tracking script for the specific custom field number.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  for ($x = 1; $x <= 6; $x++) {
    $form['custom_session_field_fieldset']['yahoo_web_analytics_custom_session_field_0'. $x] = array(
      '#title' => t('Custom Field 0'. $x),
      '#type' => 'textfield',
      '#description' => t('Populate to track value for the Yahoo! Web Analytics custom session field #0'. $x),
      '#default_value' => variable_get('yahoo_web_analytics_custom_session_field_0'. $x, ''),
      '#size' => 20,
      '#maxlength' => 75,
    );
  }

  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Specific Tracking Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $has_php_access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('yahoo_web_analytics_visibility', YAHOO_WEB_ANALYTICS_VISIBILITY);
  $visibility_paths = variable_get('yahoo_web_analytics_visibility_paths', YAHOO_WEB_ANALYTICS_VISIBILITY_PATHS);

  if ($visibility == 2 && !$has_php_access) {
    $form['page_vis_settings'] = array();
    $form['page_vis_settings']['yahoo_web_analytics_visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_vis_settings']['yahoo_web_analytics_visibility_paths'] = array('#type' => 'value', '#value' => $visibility_paths);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if ($has_php_access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['page_vis_settings']['yahoo_web_analytics_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_vis_settings']['yahoo_web_analytics_visibility_paths'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $visibility_paths,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }
  $form['advanced_tracking_settings'] = array(
    '#title' => t('Advanced Tracking Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $search_module_dependency = '<div class="admin-dependencies">';
  $search_module_dependency .= t('Depends on: !dependencies', array('!dependencies' => (module_exists('search') ? t('@module (<span class="admin-enabled">enabled</span>)', array('@module' => 'Search')) : t('@module (<span class="admin-disabled">disabled</span>)', array('@module' => 'Search')))));
  $search_module_dependency .= '</div>';

  $form['advanced_tracking_settings']['yahoo_web_analytics_internal_search'] = array(
    '#title' => t('Track internal search'),
    '#type' => 'checkbox',
    '#description' => t('If checked, internal search keywords and results count will be tracked. Also, if checked, all configurations with the INTERNAL SEARCH predefined action selected, will have the predefined action deselected.'. $search_module_dependency),
    '#default_value' => variable_get('yahoo_web_analytics_internal_search', FALSE),
    '#disabled' => module_exists('search') ? FALSE : TRUE,
  );
  $form['advanced_tracking_settings']['yahoo_web_analytics_subdomain_tracking'] = array(
    '#title' => t('Track subdomains as internal'),
    '#type' => 'checkbox',
    '#description' => t('If checked, all subdomains will not be counted as exit links, but as part of the site\'s domain.'),
    '#default_value' => variable_get('yahoo_web_analytics_subdomain_tracking', FALSE),
  );
  $form['advanced_tracking_settings']['yahoo_web_analytics_js_scope'] = array(
    '#title' => t('JavaScript Scope'),
    '#type' => 'select',
    '#description' => t('Yahoo! Web Analytics recommends adding the external JavaScript files to footer (i.e. bottom of page) for performance.'),
    '#options' => array('footer' => t('Footer'), 'header' => t('Header')),
    '#default_value' => variable_get('yahoo_web_analytics_js_scope', 'footer'),
  );

  $form['#submit'][] = 'yahoo_web_analytics_admin_settings_form_submit';

  return system_settings_form($form);

} // END admin_settings_form().


/**
 * Additional submit function for module's admin_settings_form.
 */
function yahoo_web_analytics_admin_settings_form_submit(&$form, &$form_state) {

  // If internal search tracking enabled, clear predefined INTERNAL SEARCH actions from custom configurations.
  if ($form_state['values']['yahoo_web_analytics_internal_search']) {
    db_query('UPDATE {yahoo_web_analytics} SET predefined_action_internal_search = 0');
  }

} // END yahoo_web_analytics_admin_settings_form_submit().


/**
 * Admin custom configuration search form.
 */
function configuration_search_form(&$form_state, $search_criteria = NULL) {

  $form['search_criteria'] = array(
    '#type' => 'textfield',
    '#description' => t('Search friendly names or included paths.'),
    '#size' => 35,
    '#maxlength' => 100,
    '#default_value' => $search_criteria,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;

} // END configuration_search_form().


/**
 * Validation function for configuration_search_form().
 */
function configuration_search_form_validate($form, &$form_state) {

  if ( empty($form_state['values']['search_criteria']) ) {
    form_set_error('search_criteria', t('Search criteria must be present when searching.'));
  }

} // END configuration_search_form_validate().


/**
 * Form submission function for configuration_search_form().
 */
function configuration_search_form_submit(&$form, &$form_state) {

  $form_state['redirect'] = array('admin/settings/yahoo_web_analytics/list', 'c='. $form_state['values']['search_criteria']);

} // END configuration_search_form_submit().


/**
 * Admin custom configuration form.
 */
function configuration_form(&$form_state, $yaid = NULL, $to_copy = FALSE, $read_only = FALSE) {

  // Get configuration data from database to default form values.
  if ($yaid) {
    $config = _load_configuration($yaid);
  }

  $form['included_path_fieldset'] = array(
    '#title' => t('Required Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['included_path_fieldset']['internal_name'] = array(
    '#title' => t('Friendly Name'),
    '#type' => 'textfield',
    '#description' => t('Name used within module.  Not used in tracking script.'),
    '#default_value' => isset($config) ? $config->internal_name : '',
    '#size' => 20,
    '#maxlength' => 75,
    '#required' => TRUE,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['included_path_fieldset']['include_paths'] = array(
    '#title' => t('Included Paths/Pages/URLs'),
    '#type' => 'textarea',
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard and may only be placed at the end of a path. Paths should also NOT begin with a forward slash ( / ). Example paths are blog for the blog page and blog/* for every personal blog."),
    '#default_value' => isset($config) ? $config->include_paths : '',
    '#required' => TRUE,
    '#resizable' => TRUE,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['document_name_fieldset'] = array(
    '#title' => t('Override/Edit Document Name Field'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => isset($config) && !empty($config->document_name) ? FALSE : TRUE,
  );
  $form['document_name_fieldset']['document_name'] = array(
    '#title' => t('Document Name'),
    '#type' => 'textfield',
    '#description' => t('Populate to override value for the Yahoo! Web Analytics DOCUMENT NAME field, which will be populated with the page title if left blank.'),
    '#default_value' => isset($config) ? $config->document_name : '',
    '#size' => 20,
    '#maxlength' => 75,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['document_group_fieldset'] = array(
    '#title' => t('Add/Edit Document Group Field'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['document_group_fieldset']['document_group'] = array(
    '#title' => t('Document Group'),
    '#type' => 'textfield',
    '#description' => t('Populate to include value for the Yahoo! Web Analytics DOCUMENT GROUP field.'),
    '#default_value' => isset($config) ? $config->document_group : '',
    '#size' => 20,
    '#maxlength' => 75,
    '#disabled' => $read_only ? TRUE : FALSE,
  );

  // Custom fields (PageView) 07-16.
  $form['custom_pageview_fieldset'] = array(
    '#title' => t('Add/Edit Custom Fields (PageView)'),
    '#type' => 'fieldset',
    '#description' => t('The fields below allow specific pageview values to be tracked on the specified path(s).<p><strong>Example:</strong><br />If a static value for the specified path(s) was to be tracked, the value would be placed into a field below, then properly configured within the Yahoo! Web Analytics user interface.  The pageview field value would then be placed into the tracking script for the specific custom field number.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  for ($x = 7; $x <= 16; $x++) {
    $var = 'custom_field_'. ($x < 10 ? '0'. $x : $x);
    $form['custom_pageview_fieldset']['custom_field_'. ($x < 10 ? '0'. $x : $x)] = array(
      '#title' => t('Custom Field '. ($x < 10 ? '0'. $x : $x)),
      '#type' => 'textfield',
      '#description' => t('Populate to include value for the Yahoo! Web Analytics custom pageview field #'. ($x < 10 ? '0'. $x : $x) .'.'),
      '#default_value' => isset($config) ? $config->$var : '',
      '#size' => 20,
      '#maxlength' => 75,
      '#disabled' => $read_only ? TRUE : FALSE,
    );
  }

  $form['custom_action_fieldset'] = array(
    '#title' => t('Add/Edit Custom Actions'),
    '#type' => 'fieldset',
    '#description' => t('The fields below allow specific actions to be tracked on the specified path(s). The actions are only included on a page-by-page basis, rather than on user events (e.g. onclick).  The actions selected here will be added to every path/page/URL specified under the Required Settings heading above.<p><strong>Example:</strong><br />If a custom action for the specified path(s) was to be tracked, the action should be checked, then properly configured within the Yahoo! Web Analytics Dashboard.  The custom action number would then be placed into the tracking script.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Custom actions 02-11.
  for ($x = 2; $x <= 11; $x++) {
    $var = 'custom_action_'. ($x < 10 ? '0'. $x : $x);
    $form['custom_action_fieldset']['custom_action_'. ($x < 10 ? '0'. $x : $x)] = array(
      '#title' => t('Custom Action '. ($x < 10 ? '0'. $x : $x)),
      '#type' => 'checkbox',
      '#description' => t('Check to include the Yahoo! Web Analytics custom Action #'. ($x < 10 ? '0'. $x : $x) .' action.'),
      '#default_value' => isset($config) ? $config->$var : 0,
      '#disabled' => $read_only ? TRUE : FALSE,
    );
  }

  $form['predefined_action_fieldset'] = array(
    '#title' => t('Add/Edit Predefined Actions'),
    '#type' => 'fieldset',
    '#description' => t('The fields below allow predefined actions to be tracked on the specified path(s).  The actions are included on a page-by-page basis, rather than on user events (e.g. onclick). The actions selected here will be added to every path/page/URL specified under the Required Settings heading above.<p><strong>Note:</strong> If the module\'s internal search tracking is enabled, the INTERNAL_SEARCH action will automatically be included for the related search pages.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['predefined_action_fieldset']['predefined_action_sale'] = array(
    '#title' => t('SALE'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined SALE action.'),
    '#default_value' => isset($config) ? $config->predefined_action_sale : 0,
    '#disabled' => $read_only ? TRUE : FALSE,
  );

  // Uncheck and disable INTERNAL_SEARCH action if internal search enabled on module settings.
  $default_search_value = isset($config) ? $config->predefined_action_internal_search : 0;
  $is_search_disabled = $read_only ? TRUE : FALSE;
  if ( variable_get('yahoo_web_analytics_internal_search', FALSE) ) {
    $default_search_value = 0;
    $is_search_disabled = TRUE;
  }

  $form['predefined_action_fieldset']['predefined_action_internal_search'] = array(
    '#title' => t('INTERNAL SEARCH'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined INTERNAL_SEARCH action.'),
    '#default_value' => $default_search_value,
    '#disabled' => $is_search_disabled,
  );
  $form['predefined_action_fieldset']['predefined_action_pending_sale'] = array(
    '#title' => t('PENDING_SALE'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined PENDING_SALE action.'),
    '#default_value' => isset($config) ? $config->predefined_action_pending_sale : 0,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['predefined_action_fieldset']['predefined_action_cancelled_sale'] = array(
    '#title' => t('CANCELLED_SALE'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined CANCELLED_SALE action.'),
    '#default_value' => isset($config) ? $config->predefined_action_cancelled_sale : 0,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['predefined_action_fieldset']['predefined_action_product_view'] = array(
    '#title' => t('PRODUCT_VIEW'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined PRODUCT_VIEW action.'),
    '#default_value' => isset($config) ? $config->predefined_action_product_view : 0,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['predefined_action_fieldset']['predefined_action_add_cart'] = array(
    '#title' => t('ADD_TO_CART'),
    '#type' => 'checkbox',
    '#description' => t('Check to include the Yahoo! Web Analytics predefined ADD_TO_CART action.'),
    '#default_value' => isset($config) ? $config->predefined_action_add_cart : 0,
    '#disabled' => $read_only ? TRUE : FALSE,
  );
  $form['yaid'] = array(
    '#type' => 'value',
    '#value' => isset($config) && !$to_copy ? $config->yaid : '',
  );
  if (!$read_only ) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/settings/yahoo_web_analytics/list'),
  );

  return $form;

} // END configuration_form().


/**
 * Validation function for configuration_form().
 */
function configuration_form_validate($form, &$form_state) {

  // Get yaid if present (e.g. editing a configuration).
  $form_yaid = $form_state['values']['yaid'];

  // Get array of configuration include paths from form.
  $form_paths_arr = explode("\n", $form_state['values']['include_paths']);

  // Get all previously configured paths to validate current include paths against.
  $all_paths_arr = array();
  $resultset = db_query('SELECT yaid, internal_name, include_paths FROM {yahoo_web_analytics}');
  while ($config = db_fetch_object($resultset)) {
    $path_arr = explode("\n", $config->include_paths);
    foreach ($path_arr as $path) {
      if ($form_yaid != $config->yaid) {
        $all_paths_arr[trim($path)] = $config;
      }
    }
  }

  // Validate each include path.
  foreach ($form_paths_arr as $path) {
    // Ensure form path(s) are not already in use.
    $path = trim($path);
    if ( array_key_exists($path, $all_paths_arr) ) {
      form_set_error('include_paths', t('Path "!path" already in use by !config_link custom configuration.', array('!path' => $path, '!config_link' => l($all_paths_arr[$path]->internal_name, 'admin/settings/yahoo_web_analytics/'. $all_paths_arr[$path]->yaid .'/edit'))));
    }

    // Ensure path(s) does not include embedded wildcard (i.e. *), unless it is the last path character. This
    // restriction is to help with ambiguity of determining precedence in _add_tracking_fields().
    if ( strpos($path, '*') !== FALSE && strpos($path, '*') <  drupal_strlen(trim($path)) - 1) {
      form_set_error('include_paths', t('Path "!path" may only contain wildcards at end of path.', array('!path' => $path)));
    }

    // Ensure path(s) does not begin with a forward slash.
    if ( strpos($path, '/') !== FALSE && strpos($path, '/') == 0 ) {
      form_set_error('include_paths', t('Path "!path" should not begin with a forward slash ( / ).', array('!path' => $path)));
    }
  }

} // END configuration_form_validate().


/**
 * Form submission function for configuration_form().
 */
function configuration_form_submit($form, &$form_state) {

  if ( !empty($form_state['values']['yaid']) ) {
    // Update custom configuration.
    db_query("UPDATE {yahoo_web_analytics} SET internal_name = '%s', include_paths = '%s', document_name = '%s', document_group = '%s', custom_field_07 = '%s', custom_field_08 = '%s', custom_field_09 = '%s', custom_field_10 = '%s', custom_field_11 = '%s', custom_field_12 = '%s', custom_field_13 = '%s', custom_field_14 = '%s', custom_field_15 = '%s', custom_field_16 = '%s', custom_action_02 = '%s', custom_action_03 = '%s', custom_action_04 = '%s', custom_action_05 = '%s', custom_action_06 = '%s', custom_action_07 = '%s', custom_action_08 = '%s', custom_action_09 = '%s', custom_action_10 = '%s', custom_action_11 = '%s', predefined_action_sale = '%s', predefined_action_internal_search = '%s', predefined_action_pending_sale = '%s', predefined_action_cancelled_sale = '%s', predefined_action_product_view = '%s', predefined_action_add_cart = '%s' WHERE yaid = %d", $form_state['values']['internal_name'], $form_state['values']['include_paths'], $form_state['values']['document_name'], $form_state['values']['document_group'], $form_state['values']['custom_field_07'], $form_state['values']['custom_field_08'], $form_state['values']['custom_field_09'], $form_state['values']['custom_field_10'], $form_state['values']['custom_field_11'], $form_state['values']['custom_field_12'], $form_state['values']['custom_field_13'], $form_state['values']['custom_field_14'], $form_state['values']['custom_field_15'], $form_state['values']['custom_field_16'], $form_state['values']['custom_action_02'], $form_state['values']['custom_action_03'], $form_state['values']['custom_action_04'], $form_state['values']['custom_action_05'], $form_state['values']['custom_action_06'], $form_state['values']['custom_action_07'], $form_state['values']['custom_action_08'], $form_state['values']['custom_action_09'], $form_state['values']['custom_action_10'], $form_state['values']['custom_action_11'], $form_state['values']['predefined_action_sale'], $form_state['values']['predefined_action_internal_search'], $form_state['values']['predefined_action_pending_sale'], $form_state['values']['predefined_action_cancelled_sale'], $form_state['values']['predefined_action_product_view'], $form_state['values']['predefined_action_add_cart'], $form_state['values']['yaid']);
    drupal_set_message( t('Custom configuration %internal_name was updated.', array('%internal_name' => $form_state['values']['internal_name']) ));
  }
  else {
    // Insert custom custom configuration.
    db_query("INSERT INTO {yahoo_web_analytics} (internal_name, include_paths, document_name, document_group, custom_field_07, custom_field_08, custom_field_09, custom_field_10, custom_field_11, custom_field_12, custom_field_13, custom_field_14, custom_field_15, custom_field_16, custom_action_02, custom_action_03, custom_action_04, custom_action_05, custom_action_06, custom_action_07, custom_action_08, custom_action_09, custom_action_10, custom_action_11, predefined_action_sale, predefined_action_internal_search, predefined_action_pending_sale, predefined_action_cancelled_sale, predefined_action_product_view, predefined_action_add_cart, created) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d)", $form_state['values']['internal_name'], $form_state['values']['include_paths'], $form_state['values']['document_name'], $form_state['values']['document_group'], $form_state['values']['custom_field_07'], $form_state['values']['custom_field_08'], $form_state['values']['custom_field_09'], $form_state['values']['custom_field_10'], $form_state['values']['custom_field_11'], $form_state['values']['custom_field_12'], $form_state['values']['custom_field_13'], $form_state['values']['custom_field_14'], $form_state['values']['custom_field_15'], $form_state['values']['custom_field_16'], $form_state['values']['custom_action_02'], $form_state['values']['custom_action_03'], $form_state['values']['custom_action_04'], $form_state['values']['custom_action_05'], $form_state['values']['custom_action_06'], $form_state['values']['custom_action_07'], $form_state['values']['custom_action_08'], $form_state['values']['custom_action_09'], $form_state['values']['custom_action_10'], $form_state['values']['custom_action_11'], $form_state['values']['predefined_action_sale'], $form_state['values']['predefined_action_internal_search'], $form_state['values']['predefined_action_pending_sale'], $form_state['values']['predefined_action_cancelled_sale'], $form_state['values']['predefined_action_product_view'], $form_state['values']['predefined_action_add_cart'], time());
    drupal_set_message( t('Custom configuration %internal_name was added.', array('%internal_name' => $form_state['values']['internal_name']) ));
  }

  $form_state['redirect'] = 'admin/settings/yahoo_web_analytics/list';

} // END configuration_form_submit().


/**
 * Admin view configurations callback.
 */
function view_configurations() {

  define('FIELD_CONFIGS_TO_DISPLAY', 25);

  // Create heading data for configuration listing table.
  $table_header = array(
    array('data' => t('Friendly Name'), 'field' => 'internal_name'),
    array('data' => t('Included Paths/Pages/URLs'), 'field' => 'include_paths'),
    array('data' => t('Operations'))
  );

  // Get all configurations from database.
  $query = 'SELECT yaid, internal_name, include_paths FROM {yahoo_web_analytics} ';
  $search_criteria = $_GET['c'];
  if ($search_criteria) {
    $query .= "WHERE internal_name LIKE LOWER('%%%s%') OR include_paths LIKE LOWER('%%%s%')";
    $resultset = pager_query($query . tablesort_sql($table_header), FIELD_CONFIGS_TO_DISPLAY, 0, NULL, drupal_strtolower($search_criteria), drupal_strtolower($search_criteria));
  }
  else {
    $resultset = pager_query($query . tablesort_sql($table_header), FIELD_CONFIGS_TO_DISPLAY);
  }

  // Process configuration rows and load array for configuration listing table.
  $row = array();
  while ($config = db_fetch_object($resultset)) {
    $row[] = array(
      array('data' => $config->internal_name),
      array('data' => str_replace("\n", '<br />', $config->include_paths)),
      array('data' => l( t('view'), 'admin/settings/yahoo_web_analytics/'. $config->yaid .'/view' ) .' | ' . l( t('edit'), 'admin/settings/yahoo_web_analytics/'. $config->yaid .'/edit' ) .' | ' . l( t('delete'), 'admin/settings/yahoo_web_analytics/'. $config->yaid .'/delete') .' | ' . l( t('copy'), 'admin/settings/yahoo_web_analytics/'. $config->yaid .'/copy') ),
    );
  }

  // Create configurations listing table and pager.
  if ($row) {
    // Display all or search performed and configurations found to display.

    // Add pager if necessary.
    $pager = theme('pager', NULL, FIELD_CONFIGS_TO_DISPLAY, 0);
    if ( !empty($pager) ) {
      $row[] = array(
        array('data' => $pager, 'colspan' => '3')
      );
    }

    $output = drupal_get_form('configuration_search_form', $search_criteria);
    $output .= theme('table', $table_header, $row);
  }
  elseif ($search_criteria) {
    // Search executed, but no configurations match search criteria.
    $output = drupal_get_form('configuration_search_form', $search_criteria);
    $output .= t('No configurations matched search criteria.');
  }
  else {
    // No configurations yet added.
    $output .= '<div id="yahoo_noconfig">' . t('No configurations added. !add_link.', array('!add_link' => l(t('Add configuration'), 'admin/settings/yahoo_web_analytics/add'))) . '</div>';
  }

  return $output;

} // END view_configurations().


/**
 * Delete configuration form.
 */
function yahoo_web_analytics_delete_configuration_form(&$form_state, $yaid) {

  $form['yaid'] = array(
    '#type' => 'hidden',
    '#value' => $yaid,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete this custom configuration?'),
    'admin/settings/yahoo_web_analytics/list',
    t('This cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );

} // END yahoo_web_analytics_delete_configuration_form().


/**
 * Form submission function for delete_configuration_form().
 */
function yahoo_web_analytics_delete_configuration_form_submit($form, &$form_state) {

  db_query('DELETE FROM {yahoo_web_analytics} WHERE yaid = %d', $form_state['values']['yaid']);

  drupal_set_message( t('Configuration deleted.') );
  $form_state['redirect'] = 'admin/settings/yahoo_web_analytics/list';

} // END yahoo_web_analytics_delete_configuration_form_submit().
