
CONTENTS OF THIS FILE
---------------------

 * Requirements
 * Installation
 * Custom tracking configuration
 * Other features
 * User permissions


REQUIREMENTS
------------

 Yahoo! Web Analytics account (http://web.analytics.yahoo.com/)


INSTALLATION
------------

 1. Download and extract the module's tarball (*.tar.gz archive file) into 
    your Drupal site's contributed/custom modules directory:

      /sites/all/modules

 2. Enable the module from the site's module page:
 
      Administer > Site building > Modules
      
 3. Configure the module with your site's Yahoo! Web Analytics tracking ID:
 
      Administer > Site configuration > Yahoo! Web Analytics
      
    *** Note: the site's tracking ID can be obtained from the Yahoo! Web Analytics tracking 
              script provided after creating a Yahoo! Web Analytics account and web site/project.


CUSTOM TRACKING CONFIGURATION
-----------------------------

 Custom tracking configurations allow administrators to associate various values or variables 
 to be tracked within Yahoo! Web Analytics for a given Drupal path or set of paths.  

 1. Add custom configuration(s):

      Administer > Site configuration > Yahoo! Web Analytics > Add custom configuration
      
    *** See each field's description/help text for more information.

 2. View/edit/delete/copy custom configuration(s):
      
      Administer > Site configuration > Yahoo! Web Analytics > Custom configurations
      
 *** Note: currently the module does not provide the functionality to apply tracking  
           to specific user events (e.g. onclick).
  

OTHER FEATURES
--------------

 Administrators may configure the module to track internal search keywords and search 
 result counts, as well as track sub-domains as internal domains (i.e. sub-domains links  
 not tracked as exit links). 

   Administer > Site configuration > Yahoo! Web Analytics


USER PERMISSIONS
----------------

 The module provides the user/role permission "Administer yahoo analytics", which can be granted at:

   Administer > User Management > User Permissions
